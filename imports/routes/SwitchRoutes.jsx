import React from 'react'
import {Route} from 'react-router-dom'

const SwitchRoutes = (route) =>{
    return (
        <Route 
            path={route.path} 
            render={(props)=>{
                return (<route.component {...props} routes={route.routes} xyz={'hola mundo'}/>)
            }}
        />

        
    )
}
//<Route path='/login' component={<Signin {...props} routes={route.routes} xyz={'hola mundo'} />}/>
export default SwitchRoutes
