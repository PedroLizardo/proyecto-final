import Signin from '/imports/view/auth/Signin'
import Layout from '/imports/view/layout/Layout'
import {Home,Home2} from '/imports/view/home/Home'

const routes = [
    {
        path:'/login',
        component:Signin
    },
    {
        path:'/dashboard',
        component:Layout,
        routes:[
            {
                path:"/dashboard/home",
                component:Home
            },
            {
                path:"/dashboard/home2",
                component:Home2
            },
        ]
    }
]

export default routes
