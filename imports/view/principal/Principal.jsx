import React, { Component } from 'react'
import {Link} from 'react-router-dom'
import Nav from '/imports/components/Nav'

export default class Principal extends Component {
    render() {
        //console.log(this.props)
        return (
            <div>
                <Nav link={'esto es de prinpipal'}/>
                <h1>Esta va ser mi pagina principal</h1>
                <p>donde los clientes entreran como primera vista</p>
                <Link to={'/login'}>ir a iniciar sesion</Link>
            </div>
        )
    }
}
