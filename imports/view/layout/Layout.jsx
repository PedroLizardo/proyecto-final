import React, { Component } from 'react'
import { Switch } from 'react-router-dom'
import Nav from '/imports/components/Nav'
import Sidebar from '/imports/components/Sidebar'
import SwitchRoutes from '/imports/routes/SwitchRoutes'
import Home from '/imports/view/home/Home'

export default class Layout extends Component {
    render() {
        const {routes} = this.props
        console.log(routes)
        return (
            <div>
                <Nav link={'esto es de dashboard'}/>
                <Sidebar primerlink={'hola mundo tecnologias'}/>
                <h1>Este va ser mi dashboard</h1>
                <p>luego de haber iniciado sesion</p>
                <Switch>
                    {
                        routes.map((route,i)=>{
                            return <SwitchRoutes key={i} {...route}/>
                        })
                    }
                </Switch>
            </div>
        )
    }
}
