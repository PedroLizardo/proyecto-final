import React, { Component } from 'react'
import {Link} from 'react-router-dom'

export default class Sidebar extends Component {
    render() {
        const {primerlink} = this.props
        return (
            <div>
                <h3 style={{backgroundColor:' #e53510 '}}>este sera el menu de la izquierda</h3>
                <ul>
                    <li>
                        <Link to={'/dashboard/home'}>ir a home</Link>
                    </li>
                    <li>
                        <Link to={'/dashboard/home2'}>ir a home 2</Link>
                    </li>
                    <li><a href="#">{primerlink}</a> </li>
                    <li><a href="#">link2</a> </li>
                    <li><a href="#">link3</a> </li>
                </ul>
            </div>
        )
    }
}
