import React, { Component } from 'react'
import { Meteor } from 'meteor/meteor';
import { render } from 'react-dom';
import {BrowserRouter,Route,Switch} from 'react-router-dom'
import routes from '/imports/routes/Routes'
import SwitchRoutes from '/imports/routes/SwitchRoutes'
import Principal from '/imports/view/principal/Principal'



class App extends Component {
  render() {
    return (
      <BrowserRouter>
          <Switch>
              <Route exact path="/" component={Principal}/>
              {
                routes.map((route,i)=>{//iterador 
                  return <SwitchRoutes key={i} {...route}/>
                })
              }
          </Switch>
      </BrowserRouter>
    )
  }
}


Meteor.startup(() => {
  render(<App/>, document.getElementById('aqui_se_renderizara_mi_proyecto'));
});
